from setuptools import setup, find_packages

setup(
    name='calculator',  # Required
    version='2.0.0',  # Required
    url='https://gitlab.com/JTreneL/calculator',  # Optional
    author='JTrenel',  # Optional
    author_email='Janlenert@email.cz',  # Optional
    packages=find_packages(),  # Required
    entry_points={"console_scripts": ["calculator = calculator.calculator:main"]},
    
)
